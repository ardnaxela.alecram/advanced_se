﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicPellet : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
        {
            Debug.Log("MagicStarted");
            if (FindObjectOfType<Player>())
            {
                var character = collider.GetComponent<IPlayer>();
                //Debug.Log("HandSBEFORE"+ character.handsignOfPakku.ToString());
                character.handsignOfPakku=(Random.Range(0, 3));  
                //Debug.Log("HandSAFTER"+character.handsignOfPakku.ToString());
                
                Destroy(gameObject);
            }
       
        }
    
}
