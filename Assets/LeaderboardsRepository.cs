﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardsRepository : MonoBehaviour
{
    [SerializeField] public int score;
    [SerializeField] public string playerName;
    public GameObject PlayerName;

    private  const  string pathScore ="C:\\Users\\alexandra.stober\\Desktop\\DHBW\\Unity\\Projects\\Pakkuman\\Assets\\Scripts\\CurrentScoreValue.txt";
    private  const  string pathName ="C:\\Users\\alexandra.stober\\Desktop\\DHBW\\Unity\\Projects\\Pakkuman\\Assets\\Scripts\\CurrentNameValue.txt";

    public int LoadScore()
    {

        if (File.Exists(pathScore))
        {
            return Int32.Parse(File.ReadAllText(pathScore));

        }

        return 0;
    }

    public void saveScore(int score)
    {
        File.Create(pathScore);
        File.WriteAllText(pathScore,score.ToString());
    }

    void Start()
    {
        try
        {
            gameObject.GetComponent<InputField>().GetComponent<Text>().text = playerName;
        }
        catch (NullReferenceException ex)
        {
            
        }
    }


    public void saveName()
    {
        
        File.Create(pathName);
        
        try
        {
            if (gameObject.GetComponent<InputField>().GetComponent<Text>().text != null)
            {
                playerName = gameObject.GetComponent<InputField>().GetComponent<Text>().text;
            }
        }
        catch (NullReferenceException ex)
        {
            File.WriteAllText(pathName, "Anonym");
        }
        
    
    }

    public string LoadPlayerName()
    {
        if (File.Exists(pathName))
        {
            return File.ReadAllText(pathName);

        }
        return "Anonymous";
    }

}
