﻿using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using UnityEngine;
using UnityEngine.SceneManagement;

public class rockPaperScissorsWinLogic : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        loadWinSceneWhenAllCarrotsAreEaten();


    }

    private void loadWinSceneWhenAllCarrotsAreEaten()
    {

        if (GameObject.FindWithTag("carrot") == null)
        {
            SceneManager.LoadScene(4);

        }
    }


}
