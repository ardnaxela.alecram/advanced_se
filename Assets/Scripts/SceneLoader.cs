﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    

    public void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;


        //last level and active game session -> switch to leaderboard
        if (FindObjectOfType<GameSession>() && currentSceneIndex==2)
        {
            FindObjectOfType<GameSession>().ResetGameSession();
        }
         SceneManager.LoadScene(currentSceneIndex + 1); 

       
    }
    public void LoadWinScene()
    {
        SceneManager.LoadScene(3);
    }

    public void LoadStartMenuScene()
    {
        if (FindObjectOfType<GameSession>())
        {
            FindObjectOfType<GameSession>().ResetGameSession();
        }
        SceneManager.LoadScene(0);
    }

    public void LoadCoreScene()
    {
      
        SceneManager.LoadScene(1);
       
    }

    public void LoadPlayAgainScene()
    {
        SceneManager.LoadScene(2);
    }
    
    
    public void LoadEnterNameScene()
    {
        Destroy(GetComponent<GameSession>());
        SceneManager.LoadScene(5);
    }
    public void LoadPlayAgain()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(currentSceneIndex);
    }
    public void LoadLeaderboardsScene()
    {
        if (FindObjectOfType<GameSession>())
        {
            FindObjectOfType<GameSession>().ResetGameSession();
        }
        SceneManager.LoadScene(3);
    }
    public void ExitApplication()
    {
        Application.Quit();
    }

  

}
