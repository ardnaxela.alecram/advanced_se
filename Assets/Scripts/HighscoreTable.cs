﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.IO;

public class HighscoreTable : MonoBehaviour
{
    private Transform entryContainer;
    private Transform entryTemplate;
    private List<Transform> highscoreEntryTransfromList;
    private String highscoreTable;
    private  const  string path ="C:\\Users\\alexandra.stober\\Desktop\\DHBW\\Unity\\Projects\\Pakkuman\\Assets\\Scripts\\highscoreTable.txt";

    private void Awake()
    {
        entryContainer = transform.Find("HighscoreEntryContainer");
        entryTemplate= entryContainer.Find("HighscoreEntryTemplate");
        entryTemplate.gameObject.SetActive(false);


        var highscoreEntryList = new List<HighscoreEntry>()
        {
            new HighscoreEntry {score = 4342, name = "Ali"},
            new HighscoreEntry {score = 442, name = "Ami"},
            new HighscoreEntry {score = 342, name = "Pi"},
            new HighscoreEntry {score = 4334, name = "Ai"},
            new HighscoreEntry {score = 42, name = "All"}
        };

        string jsonString = JsonUtility.ToJson(highscoreEntryList);

        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        sortEntryListByScore(highscores);
        
        highscoreEntryTransfromList = new List<Transform>();

        foreach(HighscoreEntry highscoreEntry in highscores.highscoreEntryList)
        {
            CreateHighScoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransfromList);
        }
        //File.Delete(path);
        
    }

    private void sortEntryListByScore(Highscores highscores )
    {
        for (int i=0; i < highscores.highscoreEntryList.Count; i++){
            for(int j=i+1; j< highscores.highscoreEntryList.Count; j++)
            {
                if (highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                    HighscoreEntry tmp = highscores.highscoreEntryList[i];
                    highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                    highscores.highscoreEntryList[j] = tmp;
                }
            }
        }
    }

    private string LoadHighscore()
    {
        
        if (File.Exists(path))
        {
           return File.ReadAllText(path);
            
        }

        return "{\"highscoreEntryList\":[{\"score\":1,\"name\":\"Mai\"},{\"score\":61,\"name\":\"Kai\"}," +
               "{\"score\":30,\"name\":\"Tad\"},{\"score\":82,\"name\":\"Lars\"},{\"score\":30,\"name\":\"Dan\"}]}"; 
        
    }
    private void CreateHighScoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {
        Transform entryTransform = Instantiate(entryTemplate, container);
        float templateHeight = 30f;
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryRectTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;
        string rankString = rank + ". ";
        entryTransform.Find("positionText").GetComponent<TextMeshProUGUI>().text = rankString;
        string playerName = highscoreEntry.name;
        entryTransform.Find("playerName").GetComponent<TextMeshProUGUI>().text = playerName;
        int score = highscoreEntry.score;
        entryTransform.Find("playerScore").GetComponent<TextMeshProUGUI>().text = score.ToString();
        transformList.Add(entryTransform);

    }

    public void AddHighscoreEntry(int score, string name)
    {
        //Create HighscoreEntry
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };
        //Load saved Highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);
      
        //Add new entry to Highscores
        highscores.highscoreEntryList.Add(highscoreEntry);
       
        //Save updated Highscores
        string json = JsonUtility.ToJson(highscores);
        if(File.Exists(path)){
            File.Delete(path);
        }
        File.Create(path);
        File.WriteAllText(path,json.ToString());




    }

    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }



    //represents single Highscore Entry
    
    [Serializable]
    
    private class HighscoreEntry
    {
        public int score;
        public string name;
    }

}
