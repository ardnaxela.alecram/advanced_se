﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{


   // [SerializeField] float LevelLoadDelay = 2f;
   // [SerializeField] float LevelExitSlowMotionFactor = 0.2f;
   // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D other)
    {
       LoadNextLevel();
        
    }
    public void LoadNextLevel()
    {
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }
}
