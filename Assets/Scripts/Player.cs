﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using Random = System.Random;


public class Player : MonoBehaviour, IPlayer
{
    //Unity UI Fields
    [SerializeField] TextMeshProUGUI handsignValue;
    [SerializeField] float runSpeed = 75f;
    [SerializeField] Vector2 deathkick = new Vector2(1f, 0.5f);
   
    //Player Components
    Rigidbody2D myRigidbody2D;
    Animator myAnimator;
    CapsuleCollider2D myBodyCollider;

    private Player player;
    public int Health { get; set; }
    private Vector3 startPosition;
    public int handsignOfPakku { get; set; }


    List<string> handsignsList = new List<string> { "rock", "paper", "scissors" };
    private bool isAlive = true;

    private void Awake()
    {
        player = GetComponent<Player>();
        startPosition = transform.position;
        updateHandsignValue(handsignOfPakku);
       
    }

    // Start is called before the first frame update
    void Start()
    {   
        myAnimator = GetComponent<Animator>();
        myBodyCollider = this.GetComponent<CapsuleCollider2D>();
        myRigidbody2D = GetComponent<Rigidbody2D>();
        Health = 3;
        handsignOfPakku = randomHandsign();
        updateHandsignValue(handsignOfPakku);

    }

    private int randomHandsign()
    {
        //Debug.Log("Triggered");
        Random rand = new Random();
        int randInt= rand.Next(3);
        //Debug.Log("rand="+randInt);
        return randInt;
        
    }


    // Update is called once per frame
    void Update()
    {
        if (!isAlive)
        {
            return;
        }
        RunX();
        RunY();
        FlipSprite();
        DieHazards();
        updateHandsignValue(handsignOfPakku);

    }
    private void updateHandsignValue(int handsignOfPakku)
    {
       
        handsignValue.text = handsignsList[handsignOfPakku];
     
    }

    public String getHandsignValueText()
    {
        return handsignValue.text;

    }

   


    private void OnTriggerExit2D(Collider2D collider)
    {    

        if (collider.gameObject.CompareTag("rock") && Equals(getHandsignValueText(), "scissors"))
        {
            DieEnemy();

        } else if (collider.gameObject.CompareTag("paper") && Equals(getHandsignValueText(), "rock"))
        {
            DieEnemy();
        }
        else if (collider.gameObject.CompareTag("scissors") && Equals(getHandsignValueText(), "paper"))
        {
            DieEnemy();
        }
    }

            private void RunX()
    {
        float controlThrow = Input.GetAxis("Horizontal"); // value is between -1 and +1
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidbody2D.velocity.y);
        myRigidbody2D.velocity = playerVelocity;
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidbody2D.velocity.x) > Mathf.Epsilon;
        myAnimator.SetBool("IsWalking", playerHasHorizontalSpeed);

    }
    private void DieEnemy()
    {
        myAnimator.SetTrigger("Dying");
        GetComponent<Rigidbody2D>().velocity = deathkick;
        isAlive = false;
        if (FindObjectOfType<GameSession>())
        {
            FindObjectOfType<GameSession>().ProcessPlayerDeath();
        }

    }
    private void DieHazards()
    {
        if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Hazards")))
        {
            myAnimator.SetTrigger("Dying");
            GetComponent<Rigidbody2D>().velocity = deathkick;
            isAlive = false;
            if (FindObjectOfType<GameSession>())
            {
                FindObjectOfType<GameSession>().ProcessPlayerDeath();
            }
        }

    }
    private void RunY()
    {
        float controlThrow = Input.GetAxis("Vertical");
        Vector2 playerVelocity = new Vector2(myRigidbody2D.velocity.x, controlThrow * runSpeed);
        myRigidbody2D.velocity = playerVelocity;
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidbody2D.velocity.y) > Mathf.Epsilon;
        myAnimator.SetBool("IsWalking", playerHasHorizontalSpeed);

    }
    private bool playerHasVerticalSpeed()
    {
       return  Mathf.Abs(myRigidbody2D.velocity.y) > Mathf.Epsilon;
    }
    private bool playerHasHorizontalSpeed()
    {
       return  Mathf.Abs(myRigidbody2D.velocity.x) > Mathf.Epsilon;
    }



private void FlipSprite()
    {

        if (playerHasHorizontalSpeed())
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigidbody2D.velocity.x), 1f);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        try
        {
            if (collision.CompareTag("Foreground"))
            {
                
                if (playerHasHorizontalSpeed() && playerHasVerticalSpeed())
                {
                    transform.position = transform.position + new Vector3(-(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x))*55,
                        -(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.y))*55,
                        1f);
                }
                else if (playerHasHorizontalSpeed())
                {

                    transform.position = transform.position + new Vector3(-(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x))*55,
                        GetComponent<Rigidbody2D>().velocity.y,
                        1f);
                }
                else if (playerHasVerticalSpeed())
                {
                    transform.position = transform.position + new Vector3(GetComponent<Rigidbody2D>().velocity.x,
                        -(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.y))*55,
                        1f);
                }
            }else if (collision.gameObject.CompareTag("OutsideOfMap"))
            {
                transform.position = startPosition;
            }

        }
        catch (NullReferenceException ex)
        {
        }

    }
}
