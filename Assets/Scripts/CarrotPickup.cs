﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CarrotPickup : MonoBehaviour
{

    [SerializeField] int pointsForPickup = 100;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (FindObjectOfType<GameSession>())
        {
            FindObjectOfType<GameSession>().AddToScore(pointsForPickup);
        }
        Destroy(gameObject);
        
    }
}
