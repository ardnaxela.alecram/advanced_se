﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class EnemyMovement : MonoBehaviour
{
    enum Handsigns { rock, paper, scissors }
    private EnemyReducesHealth reduce;
    [SerializeField] float moveSpeed = 1f;
    Rigidbody2D myRigidbody;
    // Start is called before the first frame update
    private void Awake()
    {   try{
            reduce = new EnemyReducesHealth();
        }
        catch (NullReferenceException ex)
        {
        }

    }

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (IsFacingRight()){
            myRigidbody.velocity = new Vector2(moveSpeed, 0f);
        }
        else
        {
            myRigidbody.velocity = new Vector2(-moveSpeed, 0f);
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {

            transform.localScale = new Vector2(-(Mathf.Sign(myRigidbody.velocity.x)), 1f);
            IPlayer character = collider.GetComponent<IPlayer>();
            destroyEnemyObject(character.handsignOfPakku, gameObject);
            try
            {
                reduce.ReduceHealthOfPlayer(character);
            }
            catch (NullReferenceException ex)
            {
            }
        }

    }
          
    private void destroyEnemyObject(int handsign, GameObject gameObject)
    {
        // Debug.Log("enum:"+Enum.GetName(typeof(Handsigns), handsign));
        // Debug.Log("HandS"+handsign);
        // Debug.Log("enemyTag"+ gameObject.tag);

        if (gameObject.CompareTag("rock") && Equals( Enum.GetName(typeof(Handsigns), handsign), "paper"))
        {
            Destroy(gameObject);

        }
        else if (gameObject.CompareTag("paper") && Equals(Enum.GetName(typeof(Handsigns), handsign), "scissors"))
        {
            Destroy(gameObject);
        }
        else if (gameObject.CompareTag("scissors") && Equals(Enum.GetName(typeof(Handsigns), handsign), "rock"))
        {
            Destroy(gameObject);
        }

    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        try
        {
            transform.localScale = new Vector2(-(Mathf.Sign(myRigidbody.velocity.x)), 1f);
        }
        catch (NullReferenceException ex)
        {

          
        }
        
    }
    bool IsFacingRight()
    {
        return transform.localScale.x > 0;
    }
}

public class EnemyReducesHealth
{
    public void ReduceHealthOfPlayer(IPlayer player)
    {
        player.Health--;
    }
 
    
}