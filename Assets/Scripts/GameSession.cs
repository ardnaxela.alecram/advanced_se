﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameSession : MonoBehaviour
{
    [SerializeField] int playerLives = 3;
    [SerializeField] int score = 0;
    [SerializeField] TextMeshProUGUI ScoreValue;
    [SerializeField] TextMeshProUGUI LivesValue;
    [SerializeField] TextMeshProUGUI PlayerNameField;
    public int scoreValue { get; set; }
    public string playerNameFieldText { get; set; }
    private void Awake()
    {
        int numberOfGameSessions = FindObjectsOfType<GameSession>().Length;
        if (numberOfGameSessions > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        LivesValue.text = playerLives.ToString();
        ScoreValue.text= score.ToString();
       // PlayerNameField.text="Anonymous";
        playerNameFieldText = "Anonymous";
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            updatePlayerName(playerNameFieldText);
        }
    }

    private void updatePlayerName(string playerName)
    {
        if (PlayerNameField == null)
        {
            return;
        }
        PlayerNameField.text = playerName;
    }

    public void AddToScore(int pointsToAdd)
    {
        score += pointsToAdd;
        scoreValue = score;
        ScoreValue.text = score.ToString();
    }

    public void ProcessPlayerDeath()
    {
        if (playerLives > 1)
        {
            TakeLife();
        }
        else
        {
            ResetGameSession();
           // ReloadGameSession();
        }
    }
    private void TakeLife()
    {
        playerLives--;
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
        LivesValue.text = playerLives.ToString();
    }

    public void ResetGameSession()
    {
        
        Destroy(gameObject);
    }
    private void ReloadGameSession()
    {
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
    
    public void SendHighScoreToLeaderboard()
    {  
        int score = gameObject.GetComponent<GameSession>().scoreValue;
        string playerName = gameObject.GetComponent<GameSession>().playerNameFieldText;
        ResetGameSession();

    }
    
   
}

