﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSubstitute;
using NUnit.Framework;


public class LoseHealth 
{
    [Test]

    public void PlayerCollidesWithEnemyAndLosesOneLive()
    {
        EnemyReducesHealth enemy = new EnemyReducesHealth();
        IPlayer player = Substitute.For<IPlayer>();
        enemy.ReduceHealthOfPlayer(player);
        Assert.AreEqual(-1, player.Health);
    }
}
